import { reflect, isClass, isFunction, isArrowFunction, getParameters } from "./reflect";
import { equal, ok, deepEqual } from "assert";

describe('getParameters()', () => {
    it('should resolve function parameters', () => {
        deepEqual(
            getParameters(function (param1, param2) {}),
            [ 'param1', 'param2' ]
        );
    });

    it('should resolve class constructor parameters', () => {
        deepEqual(
            getParameters(class {
                constructor(param1, param2) {}
            }),
            [ 'param1', 'param2' ]
        );
    });

    it('should resolve arrow function parameters', () => {
        deepEqual(getParameters((param1, param2) => null), [ 'param1', 'param2' ]);
        deepEqual(getParameters( param1 => null ), [ 'param1' ]);
    });

    it('should resolve empty parameters', () => {
        deepEqual(getParameters(function (){}), []);
        deepEqual(getParameters(class {}), []);
        deepEqual(getParameters(() => null), []);
    });
});

describe('Type checking', async () => {

    it('isArrowFunction()', () => {
        ok( isArrowFunction( () => null ) );
        ok( isArrowFunction( _ => null ) );
        ok( !isArrowFunction(function () {}) );
        ok( !isArrowFunction(class {}) );
    });
    
    it('isClass()', () => {
        ok( isClass(class {}) );
        ok( !isClass(function () {}) );
        ok( !isClass(() => null) );
    });

    it('isFunction()', () => {
        ok(isFunction(() => null));
        ok(!isFunction(class {}), 'Classes are not functions');
    });

});

describe('Reflect', () => {
    it('anonymous function with parameters', () => {
        let r = reflect(function (a,b,c) {});
        equal('', r.name); // Name
        ok(r.isFunction);  // Function
        ok(!r.isClass);    // Class
        ok(r.isAnonymous); // Anonymous
        // Parameters
        equal(r.parameters.length, 3);
        deepEqual(r.parameters, ['a', 'b', 'c']);
    });

    it('named function without parameters', () => {
        let ref = reflect(function TestFn() {});
        equal( ref.name, 'TestFn' );
        deepEqual( ref.parameters, [] );
        equal( ref.isFunction, true );
        equal( ref.isClass, false );
        equal( ref.isAnonymous, false );
    });

});