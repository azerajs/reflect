
export const CLASS_REGEX = /class\b/;

const NO_NAME_CLASS_REGEX = /class\W*\{/;
const NO_NAME_FUNC_REGEX = /function\W*\(/;
const CLASS_PARAM_REGEX = /constructor\s*\(\s*((\w+)\s*\,\s*)+\s*\)/;
const PARAM_OFFSET_REGEX = /(constructor\s*)?\(/;
const PARAM_SEPARATOR = /\s*\,\s*/;

export interface IClass {
    new (...params);
}

export function isArrowFunction(func: Function) {
    return func instanceof Function && !func.prototype;
}

/**
 * Check if a value is Class
 * @param value 
 */
export function isClass(value): value is IClass {
    return value instanceof Function && CLASS_REGEX.test(value.toString());
}

/**
 * Check if a value is function
 * @param value 
 */
export function isFunction(value): value is Function { return value instanceof Function && !CLASS_REGEX.test(value.toString()); }

export function toString( value: string | Function ): string {
    return typeof value === 'function' ? value.toString() : value;
}

export function assertFunction(value) {
    if ( typeof value !== 'function' ) throw Error(`${ typeof value } is not a function`);
}

/**
 * Get parameters of function or a class
 * @param value Function or class
 */
export function getParameters(value: Function) {
    assertFunction(value);
    let string = toString(value);
    let isArrow = !value.prototype;
    if ( isArrow )
        return string.split('=>')[0].replace(/[()]/g, '').trim().split(PARAM_SEPARATOR).filter( param => param.length > 0 );
    let parts = string.split( PARAM_OFFSET_REGEX );
    return parts[2] ? parts[2].split(')')[0].split(PARAM_SEPARATOR).filter(param => param.length > 0) : [];
}

/**
 * Reflect class or function
 * @param value Function or class to reflect
 */
export function reflect(value: Function) {
    if ( typeof value !== 'function' ) throw Error(`Reflection only allowed for Functions and Classes`);
    let toString = value.toString();
    let name = value.name;
    let isClass = CLASS_REGEX.test(toString);
    let isFunction = !isClass;
    let isAnonymous = isClass ? NO_NAME_CLASS_REGEX.test(toString) : NO_NAME_FUNC_REGEX.test(toString);
    let isArrow = !value.prototype;
    let parameters = getParameters(value);

    return { name, isClass, isFunction, isAnonymous, isArrow, parameters, toString };
}

export function has(object: object, property): boolean {
    return object.hasOwnProperty(property);
}